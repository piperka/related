{-# LANGUAGE OverloadedStrings #-}

module DB where

import Contravariant.Extras.Contrazip
import Control.Exception
import Control.Monad
import Control.Monad.Trans.Except
import Data.Functor.Contravariant
import Data.List
import Data.Vector (Vector)
import qualified Data.Vector as Vector
import Hasql.Connection
import qualified Hasql.Decoders as D
import qualified Hasql.Encoders as E
import Hasql.Session
import Hasql.Statement
import System.Exit

withDB :: (Connection -> IO ()) -> IO ()
withDB program =
  bracket
  (acquire "postgresql://kaol@/piperka")
  maybeRelease
  maybeRun
  where
    maybeRelease (Left err) = do
      putStrLn $ "DB acquire error: " ++ show err
      exitWith $ ExitFailure 1
    maybeRelease (Right conn) = release conn
    maybeRun (Left _) = return ()
    maybeRun (Right conn) = program conn

getSubscriptions
  :: Connection
  -> ExceptT QueryError IO (Vector (Vector Int))
getSubscriptions = ExceptT . run q
  where
    q = statement () $ Statement
      "SELECT array_agg(cid) FROM subscriptions JOIN users USING (uid) \
      \JOIN comics USING (cid) \
      \WHERE countme GROUP BY uid"
      E.noParams
      (D.rowVector $
       D.column . D.nonNullable $ D.array $
       D.dimension Vector.replicateM $ D.element . D.nonNullable $ fromIntegral <$> D.int4)
      True

saveRecommends
  :: [(Int, [(Int, Float)])]
  -> Connection
  -> ExceptT QueryError IO ()
saveRecommends xs = ExceptT . run q
  where
    q = do
      sql "BEGIN"
      sql "DELETE FROM related_comics"
      forM_ xs $ \(cid, xss) ->
        statement (cid, zipWith (\i (a, b) -> (i, a, b)) [1..] xss) $ Statement
        "INSERT INTO related_comics (cid, rel, tgt, score) \
        \SELECT $1, rel, tgt, score \
        \FROM UNNEST($2 :: smallint[], $3 :: int[], $4 :: float[]) AS u (rel, tgt, score)"
        encoder
        D.noResult
        True
      sql "COMMIT"
    list = E.param . E.nonNullable . E.array . E.dimension foldl'
    encoder :: E.Params (Int, [(Int, Int, Float)])
    encoder = contrazip2
      (E.param . E.nonNullable $ fromIntegral >$< E.int4)
      (unzip3 >$< contrazip3
       (list $ E.element . E.nonNullable $ fromIntegral >$< E.int2)
       (list $ E.element . E.nonNullable $ fromIntegral >$< E.int4)
       (list $ E.element . E.nonNullable $ E.float4)
      )
