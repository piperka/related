{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE LambdaCase #-}

module Related where

import Control.Concurrent
import Control.Monad
import Control.Parallel
import Control.Parallel.Strategies
import Data.Array.IO
import Data.Array.Unboxed
import Data.Array.Unsafe
import Data.List
import qualified Data.IntSet as IntSet
import qualified Data.IntMap as IntMap
import Data.Maybe (fromJust)
import Data.Tuple
import Data.Vector (Vector)
import qualified Data.Vector as V

compact
  :: Vector (Vector Int)
  -> (Int -> Maybe Int, Int -> Maybe Int, Int)
compact xs = let
  set = IntSet.fromList $ V.toList $ join xs
  n = IntSet.size set
  pairs = zip [1..] (IntSet.toAscList set)
  in ( flip IntMap.lookup $ IntMap.fromAscList pairs
     , flip IntMap.lookup $ IntMap.fromAscList $ swap <$> pairs
     , n
     )

getSubscr
  :: Vector (Vector Int)
  -> Int
  -> IO (UArray (Int, Int) Int)
getSubscr xs n = do
  subscr :: IOUArray (Int, Int) Int <- newArray ((1, 1), (n, n)) 0
  forM_ xs $ \xss -> forM_
    (do { i <- xss ; j <- xss; guard $ i /= j ; return (i, j) }) $
    \i -> readArray subscr i >>= writeArray subscr i . succ
  unsafeFreeze subscr

getRelated
  :: UArray (Int, Int) Int
  -> IO (UArray (Int, Int) Int)
getRelated subscr = do
  threads <- getNumCapabilities
  let n = snd $ snd $ bounds subscr
      sz = n `div` threads
      chunks = map (\i -> (1+(i-1)*sz, i*sz)) [1..threads]
      allWork = [(i,j) | i <- [1..n], j <- [1..n], i /= j]
  workArrs :: [IOUArray (Int, Int) Int] <- replicateM threads $
                                           newArray ((1, 1), (n, n)) 0
  done <- newEmptyMVar
  forM_ (zip workArrs chunks) $ \(related, (low, high)) -> forkIO $ do
    forM_ [low..high] $ \i -> forM_ [j | j <- [1..n], i /= j] $ \j -> do
      let scale = subscr ! (i, j)
      when (scale /= 0) $
        forM_ [k | k <- [1..n], k /= i] $
        \k -> do let i' = (i,k)
                     s = subscr ! (j,k)
                 readArray related i' >>= writeArray related i' . (+(scale*s*s))
    putMVar done ()
  allWork `pseq` replicateM_ threads (takeMVar done)
  related :: IOUArray (Int, Int) Int <- newArray ((1, 1), (n, n)) 0
  forM_ workArrs $ \x -> forM_ [1..n] $ \i -> forM_ [j | j <- [1..n], i /= j] $ \j ->
    let idx = (i, j) in
      writeArray related idx =<< (+) <$> readArray x idx <*> readArray related idx
  unsafeFreeze related

oneRec
  :: Int
  -> UArray (Int, Int) Int
  -> UArray (Int, Int) Int
  -> Int
  -> [(Int, Float)]
oneRec n subscr related i = let
  maxSub = fromIntegral $ (foldr max 0) $
           map (\j -> subscr ! (i, j)) [1..n]
  maxRel = log (1+fromIntegral ((foldr max 0) $
                                map (\j -> related ! (i, j)) [1..n]))
  fudge = log (1+maxSub)
  rel = take 10 $ sortBy (\x y -> (snd y) `compare` (snd x))
        (map (\j -> (j, (fromIntegral $ subscr ! (i, j)) -
                        (log (1 + (fromIntegral $ related ! (i, j))) /
                         maxRel * fudge * sqrt maxSub)))
          [1..n])
  in if maxRel <= 0 || (((snd $ rel !! 0) - (snd $ rel !! (min 9 (n-1))) < 0.5)
                        && (snd $ rel !! 0) < 1) then []
     else takeWhile ((>0) . snd) rel

getRecommend
  :: UArray (Int, Int) Int
  -> UArray (Int, Int) Int
  -> (Int -> Maybe Int)
  -> [(Int, [(Int, Float)])]
getRecommend subscr related fromCompact =
  let n = snd $ snd $ bounds subscr
      oneRec' = oneRec n subscr related
  in parMap rpar
     (\(i, xs) -> ( fromJust $ fromCompact i
                  , map (\(j, x) -> (fromJust $ fromCompact j, x)) xs
                  )) $
     zip [1..n] $ map (\i -> oneRec' i) [1..n]
