module Main where

import Control.Monad.Trans
import Control.Monad.Trans.Except
import Data.Maybe
import System.Exit

import DB
import Related

main :: IO ()
main = withDB $ \conn -> do
  res <- runExceptT $ do
    subscriptions <- getSubscriptions conn
    let (fromCompact, toCompact, n) = compact subscriptions
        subs' = (fmap . fmap) (fromJust . toCompact) subscriptions
    subscr <- liftIO $ getSubscr subs' n
    related <- liftIO $ getRelated subscr
    let recommend = getRecommend subscr related fromCompact
    saveRecommends recommend conn
    return ()
  either
    (\e -> print e >> exitWith (ExitFailure 1))
    (const $ return ())
    res
